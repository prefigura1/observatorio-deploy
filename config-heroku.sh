set -e
echo "Configuring heroku access"
heroku_auth_token=`heroku auth:token`
heroku_user=`curl -H "Authorization: Bearer $heroku_auth_token" \
                  -H 'Accept: application/vnd.heroku+json; version=3' \
                  https://api.heroku.com 2> /dev/null | jq -r ".email"`
echo -e "machine git.heroku.com  password $heroku_auth_token  login $heroku_user" > ~/.netrc
