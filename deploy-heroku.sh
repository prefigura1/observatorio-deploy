set -e

dir=$(pwd)

echo "Cloning branch $1"
checkout_dir="/tmp/observatorio-$(hexdump -n 16 -e '4/4 "%08X" 1 "\n"' /dev/random)"
git clone https://framagit.org/lobster/observatorio.git -b $1 $checkout_dir
cd $checkout_dir
heroku git:remote -a $2
git commit --allow-empty -m "Deploy to Heroku"
echo "Backing up DB"
heroku pg:backups capture -a $2
echo "Deploying code"
git push heroku HEAD:refs/heads/master -f
echo "Migrating DB"
heroku run rake db:migrate -x -a $2
echo "Cleaning temp files"
cd $dir
rm -rf $checkout_dir
